This app was developed using ReactJS and TailwindCSS

## Get started

Type in the following in the command line (if you are using yarn)

```
yarn
yarn start

```

(if you are using npm)

```
npm install
npm start
```

The website will be ran on port 3000

## What have been accomplished

- Implemented all the screens from Figma
- All the forms have the validator based on the requirement.
- Keep tracks of authentication status using context API

## What could have been improved

- Custom radio box for sign up page
- Better CSS effect
- Improve the home page for introduction
