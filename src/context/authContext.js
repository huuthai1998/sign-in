import React, { useReducer, useEffect } from 'react'
import Cookies from 'js-cookie'
import axios from 'axios'

const LOGIN_REQUEST = 'LOGIN_REQUEST'
const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
const LOGIN_FAIL = 'LOGIN_FAIL'
const REGISTER_REQUEST = 'REGISTER_REQUEST'
const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
const REGISTER_FAIL = 'REGISTER_FAIL'
const LOGOUT_REQUEST = 'LOGOUT_REQUEST'
const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS'
const CHECK_AUTH_SUCCESS = 'CHECK_AUTH_SUCCESS'
const CHECK_AUTH_FAIL = 'CHECK_AUTH_FAIL'

// The authentication context provider (context API)
const authReducer = (state, action) => {
  switch (action.type) {
    case REGISTER_REQUEST:
      return { ...state, isLoading: true }
    case REGISTER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        user: action.payload,
        authenticated: true,
      }
    case REGISTER_FAIL:
      return { ...state, isLoading: false, error: action.payload }
    case LOGIN_REQUEST:
      return { ...state, isLoading: true }
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        user: action.payload,
        authenticated: true,
      }
    case LOGIN_FAIL:
      var ret = {
        ...state,
        isLoading: false,
        error: action.payload,
      }
      return ret
    case LOGOUT_REQUEST:
      return { ...state, isLoading: true }
    case LOGOUT_SUCCESS:
      return {
        ...state,
        isLoading: false,
        user: undefined,
        authenticated: false,
      }
    case CHECK_AUTH_SUCCESS:
      return { ...state, isLoading: false, authenticated: true }
    case CHECK_AUTH_FAIL:
      return { ...state, isLoading: false, authenticated: false }
    default:
      return state
  }
}

const initialState = {
  isLoading: false,
  user: undefined,
  error: '',
  authenticated: false,
}

const AuthContext = React.createContext({
  logout: async () => {},
  signUp: async (user) => {},
  logIn: async (email, password, saveToken) => {},
})

export const AuthProvider = ({ children }) => {
  const [authContext, dispatch] = useReducer(authReducer, initialState)

  //Check cookie to see if the user is signed in
  useEffect(() => {
    let user = Cookies.getJSON('userInfo')
    if (user !== undefined) dispatch({ type: CHECK_AUTH_SUCCESS })
    else dispatch({ type: CHECK_AUTH_FAIL })
  }, [])

  // Login function
  const logIn = async (username, password, saveCookie) => {
    dispatch({ type: LOGIN_REQUEST, payload: { username, password } })
    var myHeaders = new Headers()
    myHeaders.append('Content-Type', 'application/json')
    var requestOpt = {
      method: 'post',
      headers: myHeaders,
      body: JSON.stringify({
        username,
        password,
      }),
    }

    fetch('https://api.v2-dev.thuocsi.vn/interview/authorization', requestOpt)
      .then((resp) => resp.json())
      .then((result) => {
        console.log('result here!', result)
        if (result.status === 'OK') {
          if (saveCookie)
            Cookies.set('userInfo', JSON.stringify(username), { expires: 14 })
          else Cookies.set('userInfo', JSON.stringify(username))
          dispatch({ type: LOGIN_SUCCESS, payload: username })
        } else {
          throw result.message
        }
      })
      .catch((err) => {
        console.log(err)
        dispatch({ type: LOGIN_FAIL, payload: err })
        alert(err)
      })
  }

  //Logout function
  const logout = async () => {
    dispatch({ type: LOGOUT_REQUEST })
    try {
      Cookies.remove('userInfo')
      dispatch({ type: LOGOUT_SUCCESS })
    } catch (err) {
      console.error(err.response.data.error)
      throw err.response.data.error
    }
  }

  // Register function
  const signUp = async (user) => {
    dispatch({ type: REGISTER_REQUEST })
    try {
      const { data } = await axios.post(
        'https://api.v2-dev.thuocsi.vn/interview/account',
        user
      )
      Cookies.set('userInfo', JSON.stringify(data))
      dispatch({ type: REGISTER_SUCCESS, payload: data })
    } catch (err) {
      dispatch({ type: REGISTER_FAIL, payload: err.response.data.message })
      console.log(err.response.data.message)
      alert(err.response.data.message)
      throw err.response.data.message
    }
  }

  return (
    <AuthContext.Provider
      value={{
        authContext,
        logout,
        signUp,
        logIn,
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}

export const useAuth = () => React.useContext(AuthContext)
