const HOME = '/'

const LOGIN = '/login'

const REGISTER = '/register'

const REGISTER_SUCCESS = '/register_success'

const RESET_PASSWORD = '/reset-password'

const ERROR = '/error'

export default {
  HOME,
  LOGIN,
  REGISTER,
  RESET_PASSWORD,
  ERROR,
  REGISTER_SUCCESS,
}
