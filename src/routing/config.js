import ErrorPage from 'components/Error/ErrorPage'
import Home from 'components/Home/Home'
import Login from 'components/Login/Login'
import Register from 'components/Register/Register'
import routePaths from 'routing/paths'
import ResetSwitch from 'components/ForgotPassword/ResetSwitch'
import RegisterSuccess from 'components/Register/RegisterSuccess'

const config = [
  {
    path: routePaths.HOME,
    exact: true,
    component: Home,
    authOnly: true,
    redirect: routePaths.LOGIN,
  },
  {
    path: routePaths.LOGIN,
    component: Login,
    unAuthOnly: true,
    redirect: routePaths.HOME,
  },
  {
    path: routePaths.REGISTER,
    component: Register,
    redirect: routePaths.HOME,
    unAuthOnly: true,
  },
  {
    path: routePaths.REGISTER_SUCCESS,
    component: RegisterSuccess,
    redirect: routePaths.REGISTER,
    authOnly: true,
  },
  {
    path: routePaths.RESET_PASSWORD,
    component: ResetSwitch,
    redirect: routePaths.HOME,
  },

  {
    path: routePaths.ERROR,
    component: ErrorPage,
  },
]

export default config
