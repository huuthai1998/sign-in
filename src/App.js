import { AuthProvider } from 'context/authContext'
import RoutingSwitch from 'routing/RoutingSwitch'
import { BrowserRouter } from 'react-router-dom'
import React from 'react'

const App = () => {
  return (
    <AuthProvider>
      <BrowserRouter>
        <RoutingSwitch />
      </BrowserRouter>
    </AuthProvider>
  )
}

export default App
