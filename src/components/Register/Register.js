import React, { useRef, useState } from 'react'
import validator from './formHandler/validator'
import useForm from './formHandler/useForm'
import './Register.css'

const Register = () => {
  //Create hook states for input fields
  const { handleChange, handleSubmit, errors } = useForm(validator)
  // Create ref for password input and confirmation input
  const passwordBox = useRef(null)
  const confirmationBox = useRef(null)
  //Toggle function for hide/show password
  const passwordToggle = (e) => {
    e.preventDefault()
    if (passwordBox.current.type === 'text')
      passwordBox.current.type = 'password'
    else passwordBox.current.type = 'text'
  }
  const confirmationToggle = (e) => {
    e.preventDefault()
    if (confirmationBox.current.type === 'text')
      confirmationBox.current.type = 'password'
    else confirmationBox.current.type = 'text'
  }

  // Submit the form
  const submitHandler = async (e) => {
    e.preventDefault()
    try {
      handleSubmit()
    } catch (err) {
      console.log(err)
    } finally {
    }
  }

  // Catch event button enter is pressed to submit
  const handleKeyPress = (e) => {
    if (e.key === 'Enter') submitHandler(e)
  }

  return (
    <div className="bg-gray-400 h-screen flex justify-center pt-24">
      <main className="w-2/3 xl:w-1/3 ">
        <form
          onSubmit={submitHandler}
          className="shadow-2xl bg-white rounded-md px-10 md:px-16 flex flex-col text-lg font-normal"
        >
          <h1 className="text-center py-8  font-semibold text-2xl">
            Đăng ký người dùng
          </h1>
          <label htmlFor="username" className="text-lg font-normal">
            Tên đăng nhập <span className="text-red-600">*</span>
          </label>
          <input
            onChange={handleChange}
            name="username"
            type="text"
            required
            placeholder="Nhập tên đặng nhập"
            className="mb-4 border border-gray-400 px-4 py-2 rounded-md"
          />
          {errors.username && (
            <p className="text-sm text-red-500">{errors.username}</p>
          )}
          <div className="grid password-grid">
            <div className="">
              <label htmlFor="password" className="text-lg font-normal">
                Mật khẩu <span className="text-red-600">*</span>
              </label>
              <div className="flex relative">
                <input
                  onChange={handleChange}
                  ref={passwordBox}
                  name="password"
                  required
                  type="password"
                  placeholder="Nhập mật khẩu"
                  className="mb-4 w-full border border-gray-400 px-4 py-2 rounded-md"
                />
                <button
                  tabIndex="-1"
                  className="absolute show-password-button focus:outline-none"
                  onClick={passwordToggle}
                >
                  <i className="far fa-eye"></i>
                </button>
              </div>
              {errors.password && (
                <p className="text-sm text-red-500">{errors.password}</p>
              )}
            </div>
            <div className="">
              <label htmlFor="confirmation" className="text-lg font-normal">
                Nhập lại mật khẩu <span className="text-red-600">*</span>
              </label>
              <div className="flex relative">
                <input
                  onChange={handleChange}
                  ref={confirmationBox}
                  required
                  name="confirmation"
                  type="password"
                  placeholder="Nhập mật khẩu"
                  className="mb-4 w-full border border-gray-400 px-4 py-2 rounded-md"
                />
                <button
                  tabIndex="-1"
                  className="absolute show-confirmation-button focus:outline-none"
                  onClick={confirmationToggle}
                >
                  <i className="far fa-eye"></i>
                </button>
              </div>
              {errors.confirmation && (
                <p className="text-sm text-red-500">{errors.confirmation}</p>
              )}
            </div>
            <div className="flex flex-col">
              <label htmlFor="gender" className="text-lg font-normal">
                Giới tính <span className="text-red-600">*</span>
              </label>
              <select
                onChange={handleChange}
                name="gender"
                className="mb-4 border border-gray-400 px-4 py-2 rounded-md"
                required
              >
                <option htmlFor="gender" value="">
                  --
                </option>
                <option htmlFor="gender" value="MALE">
                  Male
                </option>
                <option htmlFor="gender" value="FEMALE">
                  Female
                </option>
                <option htmlFor="gender" value="OTHER">
                  Other
                </option>
              </select>
              {errors.gender && (
                <p className="text-sm text-red-500">{errors.gender}</p>
              )}
            </div>
          </div>
          <div className="">Bạn là</div>
          <div className="flex items-center justify-between py-4">
            <div className="flex items-center">
              <input
                onChange={handleChange}
                type="radio"
                className="mr-2 "
                name="experience"
                value="INTERNSHIP"
                id="intern"
              />
              <label htmlFor="intern" className="text-lg font-normal">
                Thực tập
              </label>
            </div>
            <div className="flex items-center">
              <input
                onChange={handleChange}
                type="radio"
                className="mr-2 "
                name="experience"
                value="FRESHER"
                id="fresher"
              />
              <label htmlFor="fresher" className="text-lg font-normal">
                Mới tốt nghiệp
              </label>
            </div>
            <div className="flex items-center">
              <input
                onChange={handleChange}
                type="radio"
                className="mr-2 "
                name="experience"
                value="JUNIOR"
                id="experienced"
              />
              <label htmlFor="experienced" className="text-lg font-normal">
                Đã có kinh nghiệm
              </label>
            </div>
          </div>
          {errors.experience && (
            <p className="text-sm text-red-500">{errors.experience}</p>
          )}
          <label htmlFor="displayName" className="text-lg font-normal">
            Tên hiển thị <span className="text-red-600">*</span>
          </label>
          <input
            onChange={handleChange}
            name="displayName"
            type="text"
            required
            placeholder="Nhập tên hiển thị"
            className="mb-4 border border-gray-400 px-4 py-2 rounded-md"
          />
          <div className="grid email-grid">
            <div className="">
              <label htmlFor="email" className="text-lg font-normal">
                Email <span className="text-red-600">*</span>
              </label>
              <div className="flex relative">
                <input
                  onChange={handleChange}
                  required
                  name="email"
                  type="text"
                  placeholder="Nhập email"
                  className="mb-4 w-full border border-gray-400 px-4 py-2 rounded-md"
                />
              </div>
              {errors.email && (
                <p className="text-sm text-red-500">{errors.email}</p>
              )}
            </div>
            <div className="">
              <label htmlFor="phone" className="text-lg font-normal">
                Số điện thoại <span className="text-red-600">*</span>
              </label>
              <div className="flex relative">
                <input
                  onKeyPress={handleKeyPress}
                  onChange={handleChange}
                  required
                  name="phone"
                  type="text"
                  placeholder="Nhập số điện thoại"
                  className="w-full border border-gray-400 px-4 py-2 rounded-md"
                />
              </div>
              {errors.phone && (
                <p className="text-sm text-red-500">{errors.phone}</p>
              )}
            </div>
          </div>
          <button className="my-10 bg-green-400 p-2 rounded-md text-gray-200 text-center">
            Đăng ký
          </button>
        </form>
      </main>
    </div>
  )
}

export default Register
