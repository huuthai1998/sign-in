import { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import axios from 'axios'
import { useAuth } from 'context/authContext'

// Used to submit Register.js form
const useForm = (validator) => {
  const authContext = useAuth()
  var history = useHistory()
  // Values of the fields
  const [values, setValues] = useState({
    username: '',
    password: '',
    confirmation: '',
    gender: '',
    experience: '',
    displayName: '',
    email: '',
    phone: '',
  })

  const [errors, setErrors] = useState({})

  // Handle on change event for input fields
  const handleChange = (e) => {
    const { name, value } = e.target
    setValues({
      ...values,
      [name]: value,
    })
  }

  //Validate the form. If there is no error, submit it.
  const handleSubmit = async () => {
    var errorValidator = validator(values)
    setErrors(errorValidator)
    if (Object.keys(errorValidator).length === 0) {
      const data = {
        username: values.username,
        displayName: values.displayName,
        email: values.email,
        phone: values.phone,
        sex: values.gender,
        class: values.experience,
        password: values.password,
      }
      try {
        await authContext.signUp(data)
        history.push('/register_success')
      } catch (err) {
        console.log(err)
      } finally {
      }
    }
  }
  return { handleChange, values, handleSubmit, errors }
}

export default useForm
