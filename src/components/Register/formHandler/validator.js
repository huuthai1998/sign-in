// Regex for Register input fields
const validateUsername = (input) => {
  const re = /^[a-z0-9_.]{5,}$/
  return re.test(String(input))
}
const validatePassword = (input) => {
  const re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z!@#$%_\d]{8,}$/
  return re.test(String(input))
}
const validateConfirmation = (input, password) => {
  return input === password
}

const validateEmail = (input) => {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(input).toLowerCase())
}
const validateClass = (input) => {
  return input === 'INTERNSHIP' || input === 'FRESHER' || input === 'JUNIOR'
}

const validateGender = (input) => {
  return input === 'MALE' || input === 'FEMALE' || input === 'OTHER'
}
const validatePhone = (input) => {
  const re = /^[\d]{5,}$/
  return re.test(String(input))
}

export default function validateInput(values) {
  const { username, password, confirmation, gender, experience, email, phone } =
    values
  let errors = {}

  if (!validateUsername(username)) {
    errors.username =
      'Phải có ít nhất 5 kí tự. Chỉ chữ thường, số và _. được chấp nhận'
  }

  if (!validatePassword(password)) {
    errors.password =
      'Phải có ít nhất 8 kí tự. Phải có ít nhất 1 chữ thường, 1 chữ in và 1 số'
  }

  if (!validateEmail(email)) {
    errors.email = 'Định dạng email không hợp lệ'
  }

  if (!validateConfirmation(confirmation, password)) {
    errors.confirmation = 'Xác nhận mật khẩu không khớp'
  }
  if (!validateGender(gender)) {
    errors.gender = 'Giới tính không hợp lệ'
  }

  if (!validateClass(experience)) {
    errors.experience = 'Kinh nghiệm không hợp lệ'
  }
  if (!validatePhone(phone)) {
    errors.phone = 'Định dạng số điện thoại không hợp lệ'
  }

  return errors
}
