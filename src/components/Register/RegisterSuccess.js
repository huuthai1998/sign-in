import React from 'react'
import { useHistory } from 'react-router-dom'
import successIcon from '../../resources/success-icon-23187.png'
const RegisterSuccess = () => {
  let history = useHistory()
  const redirectHandler = (e) => {
    history.push('/')
  }
  return (
    <div className="bg-gray-400 h-screen flex justify-center pt-24">
      <main className="w-2/3 xl:w-1/3 text-center">
        <div className="items-center flex flex-col shadow-2xl bg-white rounded-md px-10 md:px-16 ">
          <img src={successIcon} alt="" className="w-32 h-32 mx-auto mt-16" />
          <h1 className="pt-10 font-semibold text-2xl pb-2">
            Đăng ký thành công!
          </h1>
          <p className="">Cảm ơn bạn đã hoàn thành thông tin đăng ký.</p>
          <button
            onClick={redirectHandler}
            className=" w-32 mt-10 mb-24 bg-green-400 p-2 rounded-md text-gray-200 text-center"
          >
            Về trang chủ
          </button>
        </div>
      </main>
    </div>
  )
}

export default RegisterSuccess
