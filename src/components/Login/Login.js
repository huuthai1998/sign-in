import React, { useRef, useState } from 'react'
import './Login.css'
import { useAuth } from 'context/authContext'
import { Link } from 'react-router-dom'

const Login = () => {
  const authContext = useAuth()
  // Hook states for input fields
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [saveCookie, setSaveCookie] = useState(false)
  const [error, setError] = useState('')

  // Handle on change event for input fields
  const onChangeHandler = (e) => {
    const { name, value, checked } = e.currentTarget
    if (name === 'username') setUsername(value)
    else if (name === 'password') setPassword(value)
    else if (name === 'keep-login') setSaveCookie(checked)
  }

  // Create reference for password input and confirmation input
  const passwordBox = useRef(null)

  //Toggle function for hide/show password
  const passwordToggle = (e) => {
    e.preventDefault()
    if (passwordBox.current.type === 'text')
      passwordBox.current.type = 'password'
    else passwordBox.current.type = 'text'
  }

  // Submit form handler
  const submitHandler = async (e) => {
    try {
      e.preventDefault()
      await authContext.logIn(username, password, saveCookie)
    } catch (err) {
      console.error(err)
      setError(err)
    } finally {
      console.log('LOGGED IN')
    }
  }

  // Catch event button enter is pressed to submit
  const handleKeyPress = (e) => {
    if (e.key === 'Enter') submitHandler(e)
  }

  return (
    <div className="bg-gray-400 h-screen flex justify-center pt-24">
      <main className="w-2/3 xl:w-1/3 ">
        <form
          onSubmit={submitHandler}
          className="shadow-2xl bg-white rounded-md px-10 md:px-16 flex flex-col text-lg font-normal"
        >
          <h1 className="text-center pt-16 md:py-24 font-semibold text-2xl">
            Đăng nhập
          </h1>
          {error && (
            <div className="p-2 bg-red-700 text-gray-100 text-center text-xl mb-4 rounded">
              {error}
            </div>
          )}
          <label htmlFor="username" className="text-lg font-normal">
            Tên đăng nhập
          </label>
          <input
            onChange={onChangeHandler}
            name="username"
            type="text"
            placeholder="Nhập tên đặng nhập"
            className="mb-5 border border-gray-400 px-4 py-2 rounded-md"
          />
          <label htmlFor="password" className="text-lg font-normal">
            Mật khẩu
          </label>
          <div className="flex relative">
            <input
              onKeyPress={handleKeyPress}
              onChange={onChangeHandler}
              ref={passwordBox}
              name="password"
              type="password"
              placeholder="Nhập mật khẩu"
              className="mb-5 w-full border border-gray-400 px-4 py-2 rounded-md"
            />
            <button
              className="absolute show-password-button focus:outline-none"
              onClick={passwordToggle}
            >
              <i className="far fa-eye"></i>
            </button>
          </div>
          <div className="flex md:flex-row flex-col mb-5 justify-between">
            <label className="check-container cursor-pointer">
              <input
                type="checkbox"
                className="cursor-pointer"
                name="keep-login"
                onChange={onChangeHandler}
              />
              <span className="checkmark"></span>
              <span className="ml-8">Giữ tôi luôn đăng nhập</span>
            </label>
            <Link to="/reset-password" className="text-green-400">
              Quên mật khẩu?
            </Link>
          </div>
          {error && <p className="text-red-500">{error}</p>}
          <button
            type="submit"
            className="mb-5 bg-green-400 p-2 rounded-md text-gray-200 text-center"
          >
            Đăng nhập
          </button>
          <p className="text-center mb-10">
            Bạn chưa có tài khoản?
            <Link to="/register" className="text-green-400">
              {' Đăng ký'}
            </Link>
          </p>
        </form>
      </main>
    </div>
  )
}

export default Login
