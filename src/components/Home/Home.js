import React from 'react'
import './Home.css'
import resume from '../../resources/resume.pdf'
import profilePic from '../../resources/profile.jpg'
import { useAuth } from 'context/authContext'

const Home = () => {
  const authContext = useAuth()
  return (
    <div className="h-min-screen">
      <button
        onClick={authContext.logout}
        className="float-right	mt-5 bg-red-600 p-2 rounded-md text-gray-200 text-center"
      >
        LOG OUT
      </button>
      <main className="information" id="informationSession">
        <div name="info" className="basicInfo">
          <h1 className="introduction text-3xl">
            Hi! I'm Thai Nguyen, <br /> a software developer.
          </h1>
          <h3 className="facts">Here are some facts about me:</h3>
          <div className="facts-list">
            <div className="liInfo my-2">
              <i className="fontAw fas fa-home"></i>Location: Ho Chi Minh city,
              Vietnam
            </div>
            <div className="liInfo my-2">
              <i className="fontAw fas fa-graduation-cap"></i>Education:
              Computer Science Major at UCSC
            </div>
            <div className="liInfo my-2">
              <i className="fontAw fas fa-heart"></i> Hobbies: Grinding
              Leetcode, soccer, video games
            </div>
          </div>
        </div>
        <div className="resumeWrapper">
          <a href={resume} target="_blank" rel="noopener noreferrer">
            <button className="resume focus:outline-none"> My Resume </button>
          </a>
        </div>
        <div name="photo" className="profilePictureWrapper">
          <img className="profilePicture" src={profilePic} alt="My profile" />
        </div>
      </main>
    </div>
  )
}

export default Home
