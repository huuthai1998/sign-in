import React, { useState } from 'react'
import ForgotPassword from './ForgotPassword'
import OTP from './OTP'
import ResetPassword from './ResetPassword/ResetPassword'
import ResetPasswordSuccess from './ResetPassword/ResetPasswordSuccess'

// Handle multi level component for reset password
const ResetSwitch = () => {
  const [page, setPage] = useState(0)
  const [email, setEmail] = useState('')
  const [token, setToken] = useState('')

  const setEmailCallback = (value) => {
    setEmail(value)
  }
  const setPageCallback = (value) => {
    console.log(value)
    setPage(value)
  }
  const setTokenCallback = (value) => {
    setToken(value)
  }

  if (page === 0) {
    return (
      <ForgotPassword
        setPageCallback={setPageCallback}
        setEmailCallback={setEmailCallback}
      ></ForgotPassword>
    )
  } else if (page === 1)
    return (
      <OTP
        setPageCallback={setPageCallback}
        setTokenCallback={setTokenCallback}
        email={email}
      ></OTP>
    )
  else if (page === 2)
    return (
      <ResetPassword
        email={email}
        setPageCallback={setPageCallback}
        token={token}
      ></ResetPassword>
    )
  else return <ResetPasswordSuccess></ResetPasswordSuccess>
}

export default ResetSwitch
