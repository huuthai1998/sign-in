import React, { useRef, useState } from 'react'
import axios from 'axios'

const ResetPassword = ({ email, token, setPageCallback }) => {
  // Hook states for input fields
  const [error, setError] = useState('')
  const [password, setPassword] = useState('')
  const [confirmation, setConfirmation] = useState('')

  // Handle on change event for input fields
  const onChangeHandler = (e) => {
    const { name, value } = e.currentTarget
    if (name === 'confirmation') setConfirmation(value)
    else if (name === 'password') setPassword(value)
  }

  // Catch event button enter is pressed to submit
  const handleKeyPress = (e) => {
    if (e.key === 'Enter') submitHandler(e)
  }

  // Create reference for password input and confirmation input
  const passwordBox = useRef(null)
  const confirmationBox = useRef(null)

  // Toggle function for hide/show password
  const passwordToggle = (e) => {
    e.preventDefault()
    if (passwordBox.current.type === 'text')
      passwordBox.current.type = 'password'
    else passwordBox.current.type = 'text'
  }
  const confirmationToggle = (e) => {
    e.preventDefault()
    if (confirmationBox.current.type === 'text')
      confirmationBox.current.type = 'password'
    else confirmationBox.current.type = 'text'
  }

  // Regex for password input
  const validatePassword = (input) => {
    const re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z!@#$%_\d]{8,}$/
    return re.test(String(input)) && input.length > 4
  }

  // Submit the form
  const submitHandler = async (e) => {
    e.preventDefault()
    if (!validatePassword(password))
      setError(
        'Phải có ít nhất 8 kí tự. Phải có ít nhất 1 chữ thường, 1 chữ in và 1 số'
      )
    else if (password !== confirmation) setError('Mật khẩu xác nhận không khớp')
    else {
      try {
        var { data } = await axios.put(
          'https://api.v2-dev.thuocsi.vn/interview/account/forgot-password/password',
          { email, token, password }
        )
        if (data.status !== 'OK') throw data.message
        setPageCallback(3)
      } catch (err) {
        if (err.response) {
          setError(err.response.data.message)
          console.log(err.response.data)
        } else {
          console.log(err.message)
          setError(err.message)
        }
      } finally {
      }
    }
  }
  return (
    <div className="bg-gray-400 h-screen flex justify-center pt-24">
      <main className="w-2/3 xl:w-1/3 ">
        <form
          onSubmit={submitHandler}
          className="shadow-2xl bg-white rounded-md px-10 md:px-16 flex flex-col text-lg font-normal"
        >
          <h1 className="pt-16 md:pt-24 font-semibold text-2xl pb-2">
            Tạo lại mật khẩu
          </h1>
          <p className="pb-5">Vui lòng nhập mật khẩu mới</p>
          <label htmlFor="password" className="text-lg font-normal">
            Mật khẩu mới
          </label>
          <div className="flex relative">
            <input
              onChange={onChangeHandler}
              ref={passwordBox}
              name="password"
              type="password"
              placeholder="Nhập mật khẩu"
              className="mb-5 w-full border border-gray-400 px-4 py-2 rounded-md"
            />
            <button
              className="absolute show-password-button focus:outline-none"
              onClick={passwordToggle}
              tabIndex="-1"
            >
              <i className="far fa-eye"></i>
            </button>
          </div>

          <label htmlFor="confirmation" className="text-lg font-normal">
            Nhập lại mật khẩu mới
          </label>
          <div className="flex relative">
            <input
              onKeyPress={handleKeyPress}
              onChange={onChangeHandler}
              ref={confirmationBox}
              name="confirmation"
              type="password"
              placeholder="Nhập mật khẩu"
              className="mb-5 w-full border border-gray-400 px-4 py-2 rounded-md"
            />
            <button
              tabIndex="-1"
              className="absolute show-confirmation-button focus:outline-none"
              onClick={confirmationToggle}
            >
              <i className="far fa-eye"></i>
            </button>
          </div>
          {error && <p className="text-red-500">{error}</p>}
          <button className="mb-24 bg-green-400 p-2 rounded-md text-gray-200 text-center">
            Hoàn tất
          </button>
        </form>
      </main>
    </div>
  )
}

export default ResetPassword
