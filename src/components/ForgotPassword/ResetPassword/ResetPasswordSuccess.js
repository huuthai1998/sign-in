import React from 'react'
import { useHistory } from 'react-router-dom'
import successIcon from '../../../resources/success-icon-23187.png'
const ResetPasswordSuccess = () => {
  let history = useHistory()
  const redirectHandler = (e) => {
    history.push('/login')
  }
  return (
    <div className="bg-gray-400 h-screen flex justify-center pt-24">
      <main className="w-2/3 xl:w-1/3 text-center">
        <div className="flex flex-col shadow-2xl bg-white rounded-md px-10 md:px-16 ">
          <img src={successIcon} alt="" className="w-32 h-32 mx-auto mt-16" />
          <h1 className="pt-10 font-semibold text-2xl pb-2">
            Đặt lại mật khẩu thành công!
          </h1>
          <p className="">Tuyệt vời, bạn vừa đặt lại mật khẩu thành công.</p>
          <p className="">Vui lòng đăng nhập lại để tiếp tục sử dụng.</p>
          <button
            onClick={redirectHandler}
            className="mt-10 mb-24 bg-green-400 p-2 rounded-md text-gray-200 text-center"
          >
            Đăng nhập
          </button>
        </div>
      </main>
    </div>
  )
}

export default ResetPasswordSuccess
