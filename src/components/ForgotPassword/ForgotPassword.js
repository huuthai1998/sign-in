import axios from 'axios'
import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'

// Regex for password input
const validateEmail = (input) => {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(input).toLowerCase())
}

const ForgotPassword = ({ setPageCallback, setEmailCallback }) => {
  var history = useHistory()
  // Hook states for input field
  const [email, setEmail] = useState('')
  const [error, setError] = useState('')

  // Handle on change event for input field
  const onEmailChangeHandler = (e) => {
    setEmail(e.currentTarget.value)
  }

  // Handle the go back button
  const backHandler = () => {
    history.push('/login')
  }

  // Catch event button enter is pressed to submit
  const handleKeyPress = (e) => {
    if (e.key === 'Enter') submitHandler(e)
  }

  // Submit the form
  const submitHandler = async (e) => {
    e.preventDefault()
    if (validateEmail(email)) {
      try {
        var { data } = await axios.post(
          'https://api.v2-dev.thuocsi.vn/interview/account/forgot-password/otp',
          { email }
        )
        if (data.status !== 'OK') throw data.message
        setEmailCallback(email)
        setPageCallback(1)
      } catch (err) {
        if (err.response) {
          setError(err.response.data.message)
          console.log(err.response.data)
        } else {
          console.log(err.message)
          setError(err.message)
        }
      } finally {
      }
    } else setError('Định dạng email không hợp lệ')
  }

  return (
    <div className="bg-gray-400 h-screen flex justify-center pt-24">
      <main className="w-2/3 xl:w-1/3 ">
        <form
          onSubmit={submitHandler}
          className="shadow-2xl bg-white rounded-md px-10 md:px-16 flex flex-col text-lg font-normal"
        >
          <div className="pt-10">
            <button
              tabIndex="-1"
              className="flex justify-start"
              onClick={backHandler}
            >
              <i className="fas fa-arrow-left fa-2x"></i>
            </button>
          </div>
          <h1 className="pt-10 font-semibold text-2xl pb-2">Quên mật khẩu</h1>
          <p className="pb-5">Vui lòng nhập email để nhận mã xác thực</p>
          <label htmlFor="password" className="text-lg font-normal">
            Email
          </label>
          <input
            onKeyPress={handleKeyPress}
            onChange={onEmailChangeHandler}
            name="email"
            type="text"
            placeholder="Nhập email"
            className="mb-5 w-full border border-gray-400 px-4 py-2 rounded-md"
          />
          {error && <p className="text-red-500">{error}</p>}
          <button
            type="submit"
            className="mb-24 bg-green-400 p-2 rounded-md text-gray-200 text-center"
          >
            Gửi
          </button>
        </form>
      </main>
    </div>
  )
}

export default ForgotPassword
