import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'

const OTP = ({ setPageCallback, email, setTokenCallback }) => {
  var history = useHistory()
  // Hook states for input fields
  const [otp, setOpt] = useState(new Array(5).fill(''))
  const [error, setError] = useState('')
  // Countdown timer
  const [counter, setCounter] = useState(30)

  // If the counter is > 0, countdown
  useEffect(() => {
    if (counter > 0) {
      const interval = setInterval(() => {
        setCounter((counter) => counter - 1)
      }, 1000)
      return () => clearInterval(interval)
    }
  }, [counter])

  // Back button handler
  const backHandler = (e) => {
    e.preventDefault()
    history.push('/reset-password')
    setPageCallback(0)
  }

  // Resend OTP button handler. Only enable if counter is <= 0
  const resendOTP = async (e) => {
    e.preventDefault()
    if (counter <= 0) {
      try {
        var { data } = await axios.post(
          'https://api.v2-dev.thuocsi.vn/interview/account/forgot-password/otp',
          { email }
        )
        if (data.status !== 'OK') throw data.message
      } catch (err) {
        setError(err.response.data.message)
        console.log(err.response.data)
      } finally {
        setCounter(30)
      }
    }
  }

  // Submit the OTP form
  const confirmHandler = async (e) => {
    e.preventDefault()
    var value = parseInt(otp.join(''))
    parseInt(value)
    try {
      var { data } = await axios.put(
        'https://api.v2-dev.thuocsi.vn/interview/account/forgot-password/otp',
        { email, otp: value }
      )
      if (data.status !== 'OK') throw data.message
      setTokenCallback(data.data[0])
      setCounter(0)
      setPageCallback(2)
    } catch (err) {
      if (err.response) {
        setError(err.response.data.message)
        console.log(err.response.data)
      } else {
        console.log(err.message)
        setError(err.message)
      }
    } finally {
    }
  }
  // Catch event button enter is pressed to submit
  const handleKeyPress = (e) => {
    if (e.key === 'Enter') confirmHandler(e)
  }
  // Handle on change event for input fields
  const otpOnChangeHandler = (index) => (e) => {
    const { value } = e.currentTarget
    let tempOtp = [...otp]
    tempOtp[index] = value
    setOpt(tempOtp)
    if (e.currentTarget.nextSibling) e.currentTarget.nextSibling.focus()
  }
  return (
    <div className="bg-gray-400 h-screen flex justify-center pt-24">
      <main className="w-1/3 xl:w-1/4 ">
        <form
          onSubmit={confirmHandler}
          className="shadow-2xl bg-white rounded-md px-10 xl:px-16 flex flex-col text-lg font-normal"
        >
          <div className="pt-10">
            <button className=" flex justify-start" onClick={backHandler}>
              <i className="fas fa-arrow-left fa-2x"></i>
            </button>
          </div>
          <h1 className="pt-10 font-semibold text-2xl pb-2">Xác thực OTP</h1>
          <p className="pb-5">
            Vui lòng nhập nhập mã OTP vừa được gửi đến email <br></br>
            {email}
          </p>
          <div className="flex w-full justify-between pb-5">
            {otp.map((e, index) => {
              return (
                <input
                  onKeyPress={handleKeyPress}
                  type="text"
                  name="otp"
                  className="text-center font-bold border bg-gray-400 p-2 rounded-md w-10 h-12 xl:w-12 xl:h-16"
                  maxLength="1"
                  key={index}
                  value={e}
                  onFocus={(e) => e.target.select()}
                  onChange={otpOnChangeHandler(index)}
                />
              )
            })}
          </div>

          <button
            formAction="submit"
            className="mb-5 bg-green-400 p-2 rounded-md text-gray-200 text-center"
          >
            Xác nhận
          </button>
          {error && <p className="text-red-500">{error}</p>}
          <p className="text-center mb-24 cursor-pointer">
            {'Bạn chưa nhận được mã OTP? '}{' '}
            <span className="underline text-blue-500" onClick={resendOTP}>
              {' '}
              {`Gửi lại (${counter}s)`}
            </span>
          </p>
        </form>
      </main>
    </div>
  )
}

export default OTP
